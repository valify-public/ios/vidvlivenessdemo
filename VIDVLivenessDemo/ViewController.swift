//
//  ViewController.swift
//  VIDVLivenessDemo
//
//  Created by Sayed Obaid on 14/02/2023.
//

import UIKit
import VIDVLiveness

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func startLivenessPressed(_ sender: Any) {
        fatalError("Please fill in the credentials before starting the SDK and then remove this fatal error line")
        let creds = Credentials()
        creds.bundle = ""
        creds.baseURL = ""
        creds.userName = ""
        creds.password = ""
        creds.clientID = ""
        creds.clientSecret = ""
        TokenGeneration.getToken(creds: creds) { token in
            self.startSDK(bundle: creds.bundle, token: token!, baseURL: creds.baseURL)
        }
    }
    
    func startSDK(bundle: String, token: String, baseURL: String){
        VIDVLivenessBuilder()
            .setBundleKey(bundle)
            .setAccessToken(token)
            .setBaseURL(baseURL)
            .setLanguage("en")
            .setFailTrials(5)
            .setInstructionTimer(10)
            .setNumberOfInstructions(5)
            .showErrorDialogs(false)
            .setPrimaryColor(.orange)
//            .setFrontTransactionID("") //remove the comment if you want to make facematch with OCR front transaction id
//            .setFaceMatchImage(UIImage()) //remove the comment if you want to make facematch with custom image
            .setHeaders([:])
//          .setSSLCertificate(Data()) //remove the comment if you want to make ssl pinning
            .start(vc: self, livenessDelegate: self)
    }

}
class Credentials {
    var bundle: String = ""
    var userName: String = ""
    var password: String = ""
    var clientID: String = ""
    var clientSecret: String = ""
    var baseURL: String = ""
}
extension ViewController: VIDVLivenessDelegate {
    func onLivenessResult(_ VIDVLivenessResponse: VIDVLivenessResponse) {
        switch VIDVLivenessResponse {
        case .success(VIDVLivenessResult: let VIDVLivenessResult):
            print("livenessSuccess: ", VIDVLivenessResult.livenessSuccess)
            print("recognitionSuccess: ", VIDVLivenessResult.facematchSuccess)
            print("facematchTransactionID: ", VIDVLivenessResult.facematchTransactionID)
            let selfieImageData = VIDVLivenessResult.capturedImage
        case .serviceFailure(VIDVLivenessResult: let VIDVLivenessResult, errorCode: let errorCode, errorMessage: let errorMessage):
            print("ServiceFailure")
            print("errorCode: ", errorCode)
            print("errorMessage: ", errorMessage)
        case .builderError(errorCode: let errorCode, errorMessage: let errorMessage):
            print("ServiceFailure")
            print("errorCode: ", errorCode)
            print("errorMessage: ", errorMessage)
        case .userExited(VIDVLivenessResult: let VIDVLivenessResult, step: let step):
            print("userExit")
        case .capturedActions(capturedActions: let capturedActions):
            print("Action:", capturedActions.action)
        @unknown default:
            break
        }
    }

}
